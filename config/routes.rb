Rails.application.routes.draw do
  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :contacts, except: [:new, :edit] do
        resources :addresses, only: [:create, :destroy, :update]
      end
    end
  end
end

  # constraints subdomain: 'api' do
  #   scope module: 'api' do
  #     namespace :v1 do
  #       resources :contacts
  #     end
  #   end
  # end
  # Como ficaria neste caso: api.onebitcode.com/v1/...
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.htm
