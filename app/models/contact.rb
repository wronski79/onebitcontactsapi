class Contact < ApplicationRecord
  belongs_to :user
  has_many :addresses, dependent: :destroy
  validates :name, :user, presence: true
  self.per_page = 30
end
