class Api::V1::AddressesController < Api::V1::ApiController
  before_action :set_contact, only: [:create, :update, :destroy]
  before_action :set_address, only: [:destroy, :update]
  before_action :require_authorization!, only: [:create, :update, :destroy]
  # POST api/v1/contacts/1/addresss
  def create
    @address = @contact.addresses.build(address_params)
    if @address.save
      render json: @address, status: :created
    else
      render @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE api/v1/contacts/1/adresss/1
  def destroy
    @address.destroy#code
  end

  # PATCH/PUT api/v1/contacts/1/adresss/1
  def update
    if @address.update(address_params)
      render json: @address
    else
      render @address.errors, status: :unprocessable_entity
    end
  end

  private

  def set_contact
    @contact = Contact.find(params[:contact_id])
  end

  def set_address
    @address = Address.find(params[:id])
  end

  def address_params
    params.require(:address).permit([:ad, :created_at, :updated_at])
  end

end
