module Api::V1
  class ApiController < ApplicationController
      # metodos globais
      acts_as_token_authentication_handler_for User
      before_action :require_authentication!

      private
      def require_authentication!
        throw(:warden, scope: :user) unless current_user.presence
      end

      def require_authorization!
        unless current_user == @contact.user
          render json: {}, status: :forbidden
        end
      end
  end
end
