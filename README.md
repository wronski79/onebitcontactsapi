# README

Simple API to manage a contact list according to tutorial:

[API Ruby On Rails 5: Aprenda a criar facilmente](https://onebitcode.com/api-completa-rails/)

* Ruby version
ruby-2.4.2

* Rails version
rails-5.1.6

* Main Added Gems

      gem 'rack-cors'
  
      gem 'rack-attack'
  
      gem 'devise'
  
      gem 'simple_token_authentication', '~> 1.0'
  
      gem 'will_paginate'
  
      gem 'api-pagination'

## API Endpoints

### Contacts

| Endpoint      | Description   |
| ------------- |-------------  |
| GET /api/v1/contacts.json       | List all the contacts, and associated addresses (default pagination, 30 contacts per page) |
| GET /api/v1/contacts.json?per_page=x&page=y       | List all the contacts, with customized pagination. Pagination links on the header |
| GET /api/v1/contacts/`id`         | Show contact id, and associated addresses|
| POST /api/v1/contacts           | Create a new contact|
| PATCH/PUT /api/v1/contacts/`id`   | Update contact id|
| DELETE /api/v1/contacts/`id`      | Delete contact id|

*Contacts.json also renders associated addresses*

### Addresses

| Endpoint      | Description   |
| ------------- |-------------  |
| POST /api/v1/contacts/`contact_id`/addresses            | Create a new address for contact_id|
| PATCH/PUT /api/v1/contacts/`contact_id`/addresses/`id`  | Update a new address for contact_id|
| DELETE /api/v1/contacts/`contact_id`/addresses/`id`     | Delete address for contact_id|

